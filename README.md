# Geocaching Dashboard Events Marker
## Popis
Tento doplněk zvýrazní aktuální den v kalendáři eventů na stránce přehledu a vizuálně ukáže, zdali jsou v aktuální den  eventy a pokud ano, zda jsem na některý z nich přihlášen.

Text v poznámce listingu zobrazí v neproporcionálním (programátorském) fontu.

## Homepage
http://www.gcgpx.cz/dashboardevents/

## Download
[verze 2.1](http://www.gcgpx.cz/dashboardevents/dashboardevents.user.js)

## Náhledy

### Den bez eventu
![Dnešní den je bez eventu](https://www.littleband.cz/dashboardevents/screenshots/screenshot-noevents.png "Dnešní den je bez eventu")
### Den s eventem
![Dnes je event](https://www.littleband.cz/dashboardevents/screenshots/screenshot-noattended.png "Dnes je event")
### Den s přihlášeným eventem
![Dnes je event a jsem přihlášen logem typu WA](https://www.littleband.cz/dashboardevents/screenshots/screenshot.png "Dnes je event a jsem přihlášen logem typu WA")
### Původní poznámka v listingu
![Původní poznámka v listingu](https://www.littleband.cz/dashboardevents/screenshots/screenshot-note-default.png "Původní poznámka v listingu")
### Otevřená poznámka v listingu
![Otevřená poznámka v listingu](https://www.littleband.cz/dashboardevents/screenshots/screenshot-note-open.png "Otevřená poznámka v listingu")
### Zavřená poznámka v listingu
![Zavřená poznámka v listingu](https://www.littleband.cz/dashboardevents/screenshots/screenshot-note-closed.png "Zavřená poznámka v listingu")
