// ==UserScript==
// @name		Geocaching Personal notes & Dashboard Events Marker
// @namespace	http://gcgpx.cz/dashboardevents/
// @include		http*://www.geocaching.com/account/dashboard*
// @include		https://www.geocaching.com/geocache/*
// @version		2.1
// @description	Visualize events in dashboard events calendar and make Personal notes font monospaced
// @updateURL	http://www.gcgpx.cz/dashboardevents/dashboardevents.meta.js
// @downloadURL http://www.gcgpx.cz/dashboardevents/dashboardevents.user.js
// @icon 		http://www.gcgpx.cz/dashboardevents/dashboardevents.png
// @icon64 		http://www.gcgpx.cz/dashboardevents/dashboardevents64.png
// @grant		none
// @license		GNU General Public License v2.0
// @copyright	2019-2025 Vilem Lipold (Vylda)
// @author		Vilem Lipold (Vylda)
// @run-at		document-idle
// ==/UserScript==
