# Changelog

### [2.1] - 2022-01-21
#### Changed
- GC little helper fix (botton move to right side)
- some optimalization and refactoring

### [2.0] - 2020-01-27
#### Added
- Enable/disable button

### [1.11] - 2019-11-06
#### Added
- initial commit, first public version
