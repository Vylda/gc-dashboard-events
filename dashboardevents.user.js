// ==UserScript==
// @name		Geocaching Personal notes & Dashboard Events Marker
// @namespace	http://gcgpx.cz/dashboardevents/
// @include		http*://www.geocaching.com/account/dashboard*
// @include		https://www.geocaching.com/geocache/*
// @version		2.1
// @description	Visualize events in dashboard events calendar and make Personal notes font monospaced
// @updateURL	http://www.gcgpx.cz/dashboardevents/dashboardevents.meta.js
// @downloadURL http://www.gcgpx.cz/dashboardevents/dashboardevents.user.js
// @icon 		http://www.gcgpx.cz/dashboardevents/dashboardevents.png
// @icon64 		http://www.gcgpx.cz/dashboardevents/dashboardevents64.png
// @grant		none
// @license		GNU General Public License v2.0
// @copyright	2019-2025 Vilem Lipold (Vylda)
// @author		Vilem Lipold (Vylda)
// @run-at		document-idle
// ==/UserScript==


(function() {

	const TEXTS = {
		enable: 'Zapnout zvýrazňování eventového kalendáře',
		disable: 'Vypnout zvýrazňování eventového kalendáře',
	}
	const LS_NAME = 'VyldaEnableEventsMod';
	const BUTTON_ID = 'VyldaChangeEventsCalendarButton';
	const EVENT_RULES = [
		`#EventsCalendar .events-calendar .clndr-days .day.today {
			background: linear-gradient(135deg, #ffb76b 0%,#ff7f04 10%,#ffb76b 20%,#ff7f04 30%,#ffb76b 40%,#ff7f04 50%,#ffb76b 60%,#ff7f04 70%,#ffb76b 80%,#ff7f04 90%,#ffb76b 100%) !important;
			font-weight: bold !important;
			color: black !important;
			filter: blur(1px) grayscale(0.8);
		}`,
		`#EventsCalendar .events-calendar .clndr-days .day.today.event {
			filter: blur(1px);
		}`,
		`#EventsCalendar .events-calendar .clndr-days .day.today.event.isAttended {
			filter: none;
		}`,
	];

	const BUTTON_RULES = [
		`#${BUTTON_ID} {
			background-color: #02874d;
			color: #fff;
			display: block;
			margin: 5px auto 25px;
			padding: 10px 20px;
			font-size: 10px;
			border-radius: 0.25rem;
			border: 1px solid #02874d;
			cursor: pointer;
			width: 150px;
			transition: background-color .15s ease-out,border-color .15s ease-out,color .15s ease-out;
		}`,
		`#${BUTTON_ID}:hover {
			background-color: #00b265;
			border-color: #00b265;
		}`,
		`#EventsWidget > .panel {
			margin-bottom: 5px;
		}`,
	];

	const makeStyle = (rules, styleElement) => {
		let sheet = styleElement.sheet;
		rules.forEach((rule, index) => {
			sheet.insertRule(rule, index);
		});
	};

	const style = document.createElement('style');
	style.appendChild(document.createTextNode(''));
	document.head.appendChild(style);

	let rules = [];

	const isDashboard = /\/account\/dashboard/.test(location.href);

	if (isDashboard) {
		const ls = localStorage;
		let enableEvents = ls.getItem(LS_NAME);

		const widgetPanel = document.querySelector('.layout-sidebar.sidebar-right');
		const button = document.createElement('button');
		button.id = BUTTON_ID;
		button.addEventListener('click', () => {
			if (enableEvents) {
				button.textContent = TEXTS.enable;
				ls.removeItem(LS_NAME);
				style.sheet.disabled = true;
				enableEvents = false;
			} else {
				button.textContent = TEXTS.disable;
				ls.setItem(LS_NAME, true);
				style.sheet.disabled = false;
				enableEvents = true;
			}
		});

		if (widgetPanel) {
			widgetPanel.appendChild(button);
		}

		rules = EVENT_RULES;

		const buttonStyle = document.createElement('style');
		buttonStyle.appendChild(document.createTextNode(''));
		document.head.appendChild(buttonStyle);
		makeStyle(BUTTON_RULES, buttonStyle);

		if (enableEvents) {
			button.textContent = TEXTS.disable;
			style.sheet.disabled = false;

		} else {
			button.textContent = TEXTS.enable;
			style.sheet.disabled = true;
		}
	} else {
		rules = [
			`#cacheNoteText, #cache_note textarea, #cache_note, #viewCacheNote {
				font-family: "Fira Code", "Anonymous Pro", Consolas, "Courier new", Courier, monospace !important;
			}`
		]
	}

	makeStyle(rules, style);
}());
